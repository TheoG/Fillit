/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khabbar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/13 11:05:05 by khabbar           #+#    #+#             */
/*   Updated: 2016/11/20 15:32:52 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# define BUFF_SIZE 1024
# include <string.h>
# include <stdlib.h>
# include <fcntl.h>
# include <unistd.h>
# include "../libft/libft.h"

void	print_grid(char **grid);
void	fillit(char **argv);
void	display_grid(char **s);
void	erase(char ***grid, char lettre, int size);
void	clean_grid(char **grid);

char	**init_grid();
char	**get_list(const char *s, int nbr_tetriminos);
char	**re_alloc(char **grid);

int		read_file(char *argv, char **str_tetriminos);
int		verif_file(const char *s, int *nbr_tetriminos);
int		valid_pos(char *tetri, char **grille, int i, int j);
int		solve(char ***grille, char **tetri, int k);
int		v_block(const char *s);

#endif
