# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tgros <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/19 15:52:26 by tgros             #+#    #+#              #
#    Updated: 2016/11/21 09:42:14 by tgros            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit

SRC_PATH = src/

SRC_NAME = 	clean.c create_list.c display_grid.c erase.c \
		fillit.c grid.c main.c read_file.c solve.c \
		valid_pos.c verif_file.c verif_tetri.c

OBJ = $(SRC:.c=.o)

CC = gcc

LIB = libft/libft.a	

FLG = -Wall -Werror -Wextra

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))

all: $(NAME)

$(NAME): $(OBJ)
	$(MAKE) -C libft/
	$(CC) $(FLG) $(SRC) libft/libft.a -o $(NAME)

$(LIB):
	$(MAKE) -C libft/

sub-libft:
	$(MAKE) -C libft/

sub-clean:
	$(MAKE) -C libft/ clean

sub-fclean:
	$(MAKE) -C libft/ fclean

clean: sub-clean
	/bin/rm -f $(OBJ)

fclean: clean sub-fclean
	/bin/rm -f $(NAME)

re: fclean all
