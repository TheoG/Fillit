/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khabbar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/13 13:53:30 by khabbar           #+#    #+#             */
/*   Updated: 2016/11/20 15:04:58 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

int		backslashz(const char *s, int *i, int *current_block)
{
	if (s[*i - 1] == '\0' && s[*i - 2] == '\n' && s[*i - 3] == '\n')
		return (-1);
	if (s[*i - 1] == '\0' && (*i - *current_block) % 5 == 0)
		return (-1);
	return (0);
}

void	new_block(int *i, int *current_block, int *nbr_tetriminos)
{
	(*i)++;
	(*current_block)++;
	(*nbr_tetriminos)++;
}

int		loop(int *i, int *current_block, int *nbr_tetriminos, const char *s)
{
	if ((*i - *current_block) % 5 == 0)
	{
		if (s[*i - 1] != '\n')
			return (-1);
	}
	else
	{
		if (s[*i - 1] != '.' && s[*i - 1] != '#')
		{
			if (s[*i - 2] == '\0')
			{
				if (backslashz(s, i, current_block) == -1)
					return (-1);
			}
			else
				return (-1);
		}
	}
	if ((*i - *current_block) % 20 == 0)
	{
		if (s[*i] != '\n' && s[*i] != '\0')
			return (-1);
		new_block(i, current_block, nbr_tetriminos);
	}
	return ((*nbr_tetriminos > 26) ? -1 : 0);
}

int		verif_block(const char *s, int *nbr_tetr)
{
	int i;
	int current_block;
	int	nbr_tetriminos;

	i = 1;
	current_block = 0;
	nbr_tetriminos = 0;
	while (s[i - 1])
	{
		if (loop(&i, &current_block, &nbr_tetriminos, s) == -1)
			return (-1);
		i++;
	}
	if (backslashz(s, &i, &current_block) == -1)
		return (-1);
	*nbr_tetr = nbr_tetriminos;
	return (0);
}

int		verif_file(const char *s, int *nbr_tetriminos)
{
	if (s == NULL || *s == '\0')
		return (-1);
	if (verif_block(s, nbr_tetriminos) != 0)
		return (-1);
	if (v_block(s) != 0)
		return (-1);
	return (0);
}
