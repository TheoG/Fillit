/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 12:22:46 by tgros             #+#    #+#             */
/*   Updated: 2016/11/19 14:55:33 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

void	clean_grid(char **grid)
{
	int		len;
	int		i;

	i = 0;
	len = ft_strlen(grid[0]);
	while (i < len)
	{
		free(grid[i]);
		grid[i] = NULL;
		i++;
	}
	free(*grid);
	grid = NULL;
}
