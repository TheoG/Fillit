/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_tetri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khabbar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 10:27:28 by khabbar           #+#    #+#             */
/*   Updated: 2016/11/19 14:27:48 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

void	verif_link(const char *s, int len, int *link, int *block)
{
	(*block)++;
	if (len <= 15 && len > 5)
	{
		*link += (*(s + 1) == '#') ? 1 : 0;
		*link += (*(s - 1) == '#') ? 1 : 0;
		*link += (*(s + 5) == '#') ? 1 : 0;
		*link += (*(s - 5) == '#') ? 1 : 0;
	}
	if (len > 15 || len <= 5)
	{
		*link += (*(s + 1) == '#') ? 1 : 0;
		*link += (*(s - 1) == '#') ? 1 : 0;
		if (len > 15)
			*link += (*(s + 5) == '#') ? 1 : 0;
		if (len < 5)
			*link += (*(s - 5) == '#') ? 1 : 0;
	}
}

int		v_block(const char *s)
{
	int		len;
	int		link;
	int		block;

	len = 20;
	link = 0;
	block = 0;
	while (*s)
	{
		while (len > 0)
		{
			if (*s == '#')
				verif_link(s, len, &link, &block);
			s++;
			len--;
		}
		if (link <= 4 || block != 4)
			return (-1);
		len = 20;
		block = 0;
		link = 0;
		if (*s != '\0')
			s++;
	}
	return (0);
}
