/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 12:26:18 by tgros             #+#    #+#             */
/*   Updated: 2016/11/20 10:33:09 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

int		get_delta(char **tetri, int size)
{
	int delta;

	delta = 0;
	if (!ft_isalpha(*(*tetri)))
	{
		while (*(*tetri) == '.' || *(*tetri) == '\n')
		{
			if (*(*tetri) == '.')
				delta++;
			(*tetri)++;
		}
		delta = delta == 0 ? 0 : size - 1 - (3 - delta);
	}
	return (delta);
}

void	put_piece(char *tetri, char ***grille, int i, int j)
{
	int delta;
	int size;

	delta = 0;
	size = ft_strlen(*(*grille));
	while (i < size && *tetri)
	{
		while (*tetri && j < size)
		{
			if (delta == 0)
			{
				(*grille)[i][j] = *tetri++;
				delta = get_delta(&tetri, size);
			}
			else
				delta--;
			j++;
		}
		j = 0;
		i++;
	}
}

int		anyroom(char **grid, char *tetri, int i, int j)
{
	int		size;

	size = ft_strlen(grid[0]);
	if (grid[i][j] == '.')
	{
		if (!valid_pos(tetri, grid, i, j))
		{
			put_piece(tetri, &grid, i, j);
			return (0);
		}
	}
	return (-1);
}

int		solve(char ***grille, char **tetri, int k)
{
	int	size;
	int	i;
	int	j;

	i = 0;
	size = ft_strlen(*grille[0]);
	if (!tetri[k])
		return (0);
	while (i < size)
	{
		j = 0;
		while (j < size)
		{
			if (!anyroom(*grille, tetri[k], i, j))
			{
				if (!solve(grille, tetri, k + 1))
					return (0);
				erase(grille, tetri[k][0], size);
			}
			j++;
		}
		i++;
	}
	return (1);
}
