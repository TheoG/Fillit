/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khabbar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/13 11:12:14 by khabbar           #+#    #+#             */
/*   Updated: 2016/11/19 12:43:10 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

int		read_file(char *argv, char **str_tetriminos)
{
	int		fd;
	int		rd;
	char	buff[BUFF_SIZE];
	char	*tmp;

	*str_tetriminos = ft_strnew(1);
	fd = open(argv, O_RDONLY);
	if (fd == -1)
		return (-1);
	while ((rd = read(fd, buff, BUFF_SIZE)) > 0)
	{
		tmp = (char *)malloc(sizeof(char) *
				ft_strlen(*str_tetriminos) + rd + 1);
		if (!tmp)
		{
			free(str_tetriminos);
			exit(1);
		}
		ft_strcpy(tmp, *str_tetriminos);
		ft_strcpy(tmp + ft_strlen(*str_tetriminos), buff);
		free(*str_tetriminos);
		*str_tetriminos = tmp;
		ft_strclr(buff);
	}
	return (rd);
}
