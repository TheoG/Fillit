/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_pos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 15:09:52 by tgros             #+#    #+#             */
/*   Updated: 2016/11/19 15:26:37 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

int		check_delta(char **tetri, int size, int i)
{
	int delta;

	delta = 0;
	if (!ft_isalpha(*(*tetri)))
	{
		while (*(*tetri) == '.' || *(*tetri) == '\n')
		{
			if (*(*tetri) == '.')
				delta++;
			if (*(*tetri) == '\n' && i == size - 1)
				return (-1);
			(*tetri)++;
		}
		delta = delta == 0 ? 0 : size - 1 - (3 - delta);
	}
	return (delta);
}

int		get_nb_contigu(char *tetri)
{
	int nb_contigu;

	nb_contigu = 0;
	while (*tetri && *tetri != '.' && *tetri != '\n')
	{
		tetri++;
		nb_contigu++;
	}
	return (nb_contigu);
}

int		valid_pos(char *tetri, char **grille, int i, int j)
{
	int delta;
	int size;

	delta = 0;
	size = ft_strlen(*grille);
	while (i < size && *tetri)
	{
		while (j < size && *tetri)
		{
			if (delta == 0)
			{
				if (grille[i][j] != '.' || j + get_nb_contigu(tetri) > size)
					return (-1);
				tetri++;
				if ((delta = check_delta(&tetri, size, i)) == -1)
					return (-1);
			}
			else
				delta--;
			j++;
		}
		j = 0;
		i++;
	}
	return (0);
}
