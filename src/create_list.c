/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 13:30:40 by tgros             #+#    #+#             */
/*   Updated: 2016/11/20 11:43:48 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

char	*ft_get_clean_str(char const *s, unsigned int start, size_t len, int k)
{
	size_t	i;
	size_t	tmp;
	int		nbr_backslash;
	char	*clear_str;
	char	*ptr;

	i = start;
	nbr_backslash = 0;
	clear_str = NULL;
	tmp = len;
	clear_str = (char *)malloc(sizeof(char) * tmp + 1);
	if (!clear_str)
		return (NULL);
	ptr = clear_str;
	if (!clear_str)
		return (NULL);
	i = start;
	while (tmp--)
	{
		*clear_str = (s[i] == '.' || s[i] == '\n') ? s[i] : k + 65;
		clear_str++;
		i++;
	}
	*clear_str = '\0';
	return (ptr);
}

char	*ft_strtrim_char(char const *s, char c, int k)
{
	int		beg;
	int		end;
	int		i;
	int		len;

	beg = 0;
	end = 0;
	i = -1;
	len = 20;
	while (s[++i] == c || s[i] == '\n')
		beg++;
	i = 1;
	while (s[len - i] == c || s[len - i] == '\n')
	{
		end++;
		i++;
	}
	return (ft_get_clean_str(s, beg, len - beg - end, k));
}

char	**get_list(const char *s, int nbr_tetriminos)
{
	char	**tab;
	int		i;
	int		j;

	i = 0;
	j = 0;
	tab = (char **)malloc(sizeof(char *) * nbr_tetriminos + 1);
	if (!tab)
		return (NULL);
	while (s[j])
	{
		tab[i] = ft_strtrim_char(s + j, '.', i);
		if (!tab[i])
		{
			while (i--)
				free(tab[i]);
			free(tab);
		}
		i++;
		j += 20;
		if (s[j] == '\n')
			j++;
	}
	tab[nbr_tetriminos] = NULL;
	return (tab);
}
