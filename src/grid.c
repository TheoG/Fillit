/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 09:08:10 by tgros             #+#    #+#             */
/*   Updated: 2016/11/20 15:24:43 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

char	**re_alloc(char **grid)
{
	int		size;
	int		i;
	char	**tmp;

	i = 0;
	size = ft_strlen(grid[0]);
	tmp = (char **)malloc(sizeof(char *) * (size + 2));
	if (!tmp)
	{
		clean_grid(grid);
		exit(1);
	}
	while (i < size + 1)
	{
		tmp[i] = (char *)malloc(sizeof(char) * (size + 2));
		ft_memset(tmp[i], '.', size + 1);
		tmp[i][size + 1] = '\0';
		i++;
	}
	clean_grid(grid);
	grid = tmp;
	return (grid);
}

char	**init_grid(void)
{
	char	**tab;
	int		nb;

	nb = 0;
	tab = (char **)malloc(sizeof(char *) * 3);
	if (!tab)
		exit(1);
	while (nb < 2)
	{
		tab[nb] = (char *)malloc(sizeof(char) * 3);
		if (!tab[nb])
		{
			while (nb--)
				free(tab[nb]);
			free(tab);
			exit(1);
		}
		ft_memset(tab[nb], '.', 2);
		tab[nb][3] = '\0';
		nb++;
	}
	tab[2] = 0;
	return (tab);
}
