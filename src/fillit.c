/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: khabbar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/13 10:44:54 by khabbar           #+#    #+#             */
/*   Updated: 2016/11/20 15:22:49 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fillit.h"

void	fillit(char **argv)
{
	char	*str_tetriminos;
	char	**grid;
	char	**tetriminos_list;
	int		nbr_tetriminos;

	str_tetriminos = NULL;
	if (read_file(argv[1], &str_tetriminos) == -1 ||
		verif_file(str_tetriminos, &nbr_tetriminos) == -1)
	{
		ft_putendl("error");
		exit(1);
	}
	tetriminos_list = get_list(str_tetriminos, nbr_tetriminos);
	grid = init_grid();
	while (solve(&grid, tetriminos_list, 0))
		grid = re_alloc(grid);
	display_grid(grid);
	clean_grid(grid);
}
